#include <raylib.h>
#include <stdio.h>
#include <stdlib.h>
#include "rlgl.h"
#include "include/cell_lib.h"
#include <string.h>

void free_board(Board *  b) {
    free(b);
}

Board * alloc_board(void){
    Board * b = (Board *)malloc(sizeof(Board));
    return b;
}

void draw_board(Board * b, Texture2D * textures){
    size_t key = 0;
        for (size_t i = 0; i < 8; i++) {
         for (size_t j = 0; j < 8; j++) {
             
             DrawRectangle(b->squares[i][j].x_origin, b->squares[i][j].y_origin, 100, 100, b->squares[i][j].cell_color);

             if (b->squares[i][j].piece_type != Empty) {
                 key += b->squares[i][j].piece_type;
                 key += b->squares[i][j].piece_color;
                 DrawTexture(textures[key], b->squares[i][j].x_origin, b->squares[i][j].y_origin, WHITE);
                 key = 0;
             }

             if (b->squares[i][j].marked == 1) {
                 DrawCircle(b->squares[i][j].x_origin + 50, b->squares[i][j].y_origin + 50, 10, GREEN);
             }
         }
    }
}

void clear_marked_cell(Board *  b) {
    for (size_t i = 0; i < 8; i++) {
        for (size_t j = 0; j < 8; j++) {
            b->squares[i][j].marked = 0;
        }
    }
}
/*void clear_two_sq_jp_cell(Board b) {
    for (size_t i = 0; i < 8; i++) {
        for (size_t j = 0; j < 8; j++) {
            b.squares[i][j].two_square_jumped = 0;
        }
    }
}*/
