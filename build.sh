#!/bin/sh

set -xe

# gcc -Wall -Wextra -fsanitize=address,undefined -o main main.c -Iinclude -lraylib -lm 

# gcc -Wall -Wextra -fsanitize=address,undefined -o cell_lib.o cell_lib.c  
#
#
FLAGS="-Wall -Wextra -fsanitize=address,undefined -Iinclude"
LIBS="-lraylib -lm"
OUT="build"
BIN="bin"

gcc $FLAGS -c -o $OUT/cell_lib.o cell_lib.c
gcc $FLAGS -c -o $OUT/init.o init.c
gcc $FLAGS -c -o $OUT/legality_checker.o legality_checker.c
gcc $FLAGS -c -o $OUT/check_checker.o check_checker.c
gcc $FLAGS -o $OUT/chess $OUT/cell_lib.o $OUT/init.o $OUT/legality_checker.o $OUT/check_checker.o main.c $LIBS
# gcc $FLAGS -o $OUT/chess $OUT/cell_lib.o $OUT/init.o main.c $LIBS
