#include <raylib.h>
#include <stdbool.h>
#include <stdio.h>
#include <strings.h>
#include "rlgl.h"
#include "include/cell_lib.h"
#include "include/init.h"
#include "include/legality_checker.h"
#include "include/check_checker.h"

int screenSize = 1000;

int main(void) {

    Piece_color playing_color = White;

    _Bool holding = false;
    int cell_size = screenSize / 10;
    Piece_type temp_pt = Empty;
    Piece_color temp_pc = Void;
    int temp_xo;
    int temp_yo;

    InitWindow(screenSize, screenSize, "raylib chessboard");

    Texture2D *texture_array = alloc_texture();
    init_texture(texture_array);

    Board *  board = alloc_board();
    init_board(board, 1);

    SetTargetFPS(60);

    while (!WindowShouldClose()) {
        BeginDrawing();

        ClearBackground(RAYWHITE);
        draw_board(board, texture_array);
        if (playing_color == 0) {
            DrawText("White to play.", 325, 20, 50, BLACK);
        } else {

            DrawText("Black to play.", 325, 20, 50, BLACK);
        }

        EndDrawing();

        if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) {
            int x_coord = (GetMouseX() - (GetMouseX() % cell_size)) / cell_size;
            int y_coord = 9 - (GetMouseY() - (GetMouseY() % cell_size)) / cell_size;
            if (x_coord == 9 || x_coord == 0 || y_coord == 9 || y_coord == 0) {
                printf("outside\n");
                fflush(stdout);
            } else {
                if (!holding) {
                    if (board->squares[x_coord - 1][y_coord - 1].piece_color == playing_color) {
                        // un mark cells
                        clear_marked_cell(board);

                        holding = true;
                        temp_pt = board->squares[x_coord - 1][y_coord - 1].piece_type;
                        temp_pc = board->squares[x_coord - 1][y_coord - 1].piece_color;
                        temp_xo = x_coord - 1;
                        temp_yo = y_coord - 1;

                        gen_returner *legal_moves = gen_piece_move(board, x_coord - 1, y_coord - 1, playing_color);

                        if (legal_moves != NULL) {

                            for (int i = 0; i < legal_moves->count; i++) {
                                if (is_king_safe(board, playing_color, x_coord - 1, y_coord - 1,
                                                 legal_moves->data[i].xt, legal_moves->data[i].yt) == 1) {
                                    board->squares[legal_moves->data[i].xt][legal_moves->data[i].yt].marked = 1;
                                }
                            }
                            free_gen_returner(legal_moves);
                        }
                    }
                } else {
                    // un marck cells and execute the movement
                    if (((x_coord - 1 != temp_xo) || (y_coord - 1 != temp_yo)) &&
                        board->squares[x_coord - 1][y_coord - 1].marked == 1) {

                        holding = false;
                        board->squares[x_coord - 1][y_coord - 1].piece_type = temp_pt;
                        board->squares[x_coord - 1][y_coord - 1].piece_color = temp_pc;

                        board->squares[temp_xo][temp_yo].piece_type = Empty;
                        board->squares[temp_xo][temp_yo].piece_color = Void;
                        temp_pt = Empty;
                        temp_pc = Void;

                        int orientation;

                        if (playing_color == 0) {
                            orientation = 1;
                        } else {
                            orientation = -1;
                        }

                        if (playing_color == 0) {
                            playing_color = 6;
                        } else {
                            playing_color = 0;
                        }

                        clear_marked_cell(board);
                    } else if ((x_coord - 1 == temp_xo) && (y_coord - 1 == temp_yo)) {
                        holding = false;
                        temp_pt = Empty;
                        temp_pc = Void;

                        clear_marked_cell(board);
                        // Unmark cells
                    } else if (board->squares[x_coord - 1][y_coord - 1].piece_color == playing_color) {
                        // Un marck cells
                        clear_marked_cell(board);

                        holding = true;
                        temp_pt = board->squares[x_coord - 1][y_coord - 1].piece_type;
                        temp_pc = board->squares[x_coord - 1][y_coord - 1].piece_color;
                        temp_xo = x_coord - 1;
                        temp_yo = y_coord - 1;
                        gen_returner *legal_moves = gen_piece_move(board, x_coord - 1, y_coord - 1, playing_color);

                        if (legal_moves != NULL) {
                            for (int i = 0; i < legal_moves->count; i++) {
                                if (is_king_safe(board, playing_color, x_coord - 1, y_coord - 1,
                                                 legal_moves->data[i].xt, legal_moves->data[i].yt) == 1) {
                                    board->squares[legal_moves->data[i].xt][legal_moves->data[i].yt].marked = 1;
                                }
                            }
                            free_gen_returner(legal_moves);
                        }
                    }
                }
            }
        }
    }

    CloseWindow();

    free_board(board);
    free_texture(texture_array);

    return 0;
}
