#include <raylib.h>
#include "cell_lib.h"
#pragma once

void init_texture(Texture2D*);
Texture2D* alloc_texture(void);
void free_texture(Texture2D*);

void init_board(Board * b, int specifier);
