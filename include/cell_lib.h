#include <raylib.h>
#pragma once

enum Piece_type {

    Pawn = 0,
    Knight = 1,
    Bishop = 2,
    Rook = 3,
    Queen = 4,
    King = 5,

    Empty,
};

enum Piece_color {
    White = 0,
    Black = 6,

    Void,
};

struct Cell {
    int x_origin;
    int y_origin;
    enum Piece_type piece_type;
    enum Piece_color piece_color;
    Color cell_color;
    int marked;
    // int two_square_jumped;
    // int EPing_square;
    // _Bool occupied;
};

struct Board {
    struct Cell squares[8][8];

};

typedef struct Cell Cell;
typedef struct Board Board;
typedef enum Piece_color Piece_color;
typedef enum Piece_type Piece_type;

Board * alloc_board(void);
void free_board(Board * b);
void draw_board(Board * b, Texture2D * textures);

void clear_marked_cell(Board * b);
void clear_two_sq_jp_cell(Board b);
