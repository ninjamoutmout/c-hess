#pragma once
#include <stdlib.h>
#include "cell_lib.h"



int is_king_safe(Board * b, Piece_color king_color, int xf, int yf, int xt, int yt);
int is_square_safe(Board * b, Piece_color own_color, int x, int y);
