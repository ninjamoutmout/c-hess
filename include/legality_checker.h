#pragma once
#include "cell_lib.h"
#include <stdlib.h>

struct move_info {
    size_t xt;
    size_t yt;
};

struct Dir {
    int x, y;
};

typedef struct Dir Dir;
typedef struct move_info move_info;

// const Dir orthogonal[4] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
// const Dir diagonal[4] = {{1, 1}, {-1, -1}, {1, -1}, {-1, 1}};
// const Dir knight_magic[8] = {{2, 1}, {2, -1}, {-2, 1}, {-2, -1}, {1, 2}, {1, -2}, {-1, 2}, {-1, -2}};

struct gen_returner {
    int count;
    move_info data[];
};

typedef struct move_info move_info;
typedef struct gen_returner gen_returner;

// int is_move_valid(int xf, int yf, int xt, int yt, Board * b, Piece_color playing_color);
gen_returner *gen_piece_move(Board * b, int xf, int yf, Piece_color playing_color);
void free_gen_returner(gen_returner *ret);
// void free_gen_returner(gen_returner g);
