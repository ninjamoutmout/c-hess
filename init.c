#include "include/cell_lib.h"
#include <raylib.h>
#include <stdlib.h>

void init_board(Board * b, int specifier) {

    Color dark = (Color){140, 162, 173, 255};
    Color light = (Color){222, 227, 230, 255};

    for (int i = 0; i < 8; i++){
        for (int j = 0; j<8; j++){
            b->squares[i][j].x_origin = i*100+100;
            b->squares[i][j].y_origin = 1000 - (j * 100 + 200);
            b->squares[i][j].marked = 0;
            b->squares[i][j].piece_type = Empty;
            b->squares[i][j].piece_color = Void;

            if ((j+i)%2 == 0){
                b->squares[i][j].cell_color = dark;
            } else {
                b->squares[i][j].cell_color = light;
            }
        }
    } 
    switch (specifier) {
    case 0:
        break;
    case 1:
        b->squares[0][7].piece_type = King;
        b->squares[0][7].piece_color = White;

        b->squares[7][7].piece_type = Knight;
        b->squares[7][7].piece_color = White;

        b->squares[0][0].piece_type = Bishop;
        b->squares[0][0].piece_color = Black;

        b->squares[4][1].piece_type = Pawn;
        b->squares[4][1].piece_color = White;

        b->squares[5][3].piece_type = Pawn;
        b->squares[5][3].piece_color = Black;

        b->squares[3][3].piece_type = King;
        b->squares[3][3].piece_color = Black;

        b->squares[2][6].piece_type = Rook;
        b->squares[2][6].piece_color = White;
        break;
    }
}

void free_texture(Texture2D *t) { free(t); }

Texture2D *alloc_texture(void) {
    Texture2D *texture_array = (Texture2D *)malloc(sizeof(Texture2D) * 12);
    if (texture_array == NULL) {
        return NULL;
    } else {
        return texture_array;
    }
}

void init_texture(Texture2D *texture_array) {
    int cell_size = 100;

    Image wp = LoadImage("Ressources/Chess_pieces_426/wp.png");
    ImageResize(&wp, cell_size, cell_size);
    texture_array[0] = LoadTextureFromImage(wp);
    UnloadImage(wp);

    Image wn = LoadImage("Ressources/Chess_pieces_426/wn.png");
    ImageResize(&wn, cell_size, cell_size);
    texture_array[1] = LoadTextureFromImage(wn);
    UnloadImage(wn);

    Image wb = LoadImage("Ressources/Chess_pieces_426/wb.png");
    ImageResize(&wb, cell_size, cell_size);
    texture_array[2] = LoadTextureFromImage(wb);
    UnloadImage(wb);

    Image wr = LoadImage("Ressources/Chess_pieces_426/wr.png");
    ImageResize(&wr, cell_size, cell_size);
    texture_array[3] = LoadTextureFromImage(wr);
    UnloadImage(wr);

    Image wq = LoadImage("Ressources/Chess_pieces_426/wq.png");
    ImageResize(&wq, cell_size, cell_size);
    texture_array[4] = LoadTextureFromImage(wq);
    UnloadImage(wq);

    Image wk = LoadImage("Ressources/Chess_pieces_426/wk.png");
    ImageResize(&wk, cell_size, cell_size);
    texture_array[5] = LoadTextureFromImage(wk);
    UnloadImage(wk);

    Image bp = LoadImage("Ressources/Chess_pieces_426/bp.png");
    ImageResize(&bp, cell_size, cell_size);
    texture_array[6] = LoadTextureFromImage(bp);
    UnloadImage(bp);

    Image bn = LoadImage("Ressources/Chess_pieces_426/bn.png");
    ImageResize(&bn, cell_size, cell_size);
    texture_array[7] = LoadTextureFromImage(bn);
    UnloadImage(bn);

    Image bb = LoadImage("Ressources/Chess_pieces_426/bb.png");
    ImageResize(&bb, cell_size, cell_size);
    texture_array[8] = LoadTextureFromImage(bb);
    UnloadImage(bb);

    Image br = LoadImage("Ressources/Chess_pieces_426/br.png");
    ImageResize(&br, cell_size, cell_size);
    texture_array[9] = LoadTextureFromImage(br);
    UnloadImage(br);

    Image bq = LoadImage("Ressources/Chess_pieces_426/bq.png");
    ImageResize(&bq, cell_size, cell_size);
    texture_array[10] = LoadTextureFromImage(bq);
    UnloadImage(bq);

    Image bk = LoadImage("Ressources/Chess_pieces_426/bk.png");
    ImageResize(&bk, cell_size, cell_size);
    texture_array[11] = LoadTextureFromImage(bk);
    UnloadImage(bk);
}
