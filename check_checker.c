#include "include/cell_lib.h"
#include "include/legality_checker.h"
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Location {
    int x;
    int y;
};

typedef struct Location Location;

Location king_finder(Board * b, Piece_color king_color) {
    Location king_coord;
    for (size_t i = 0; i < 8; i++) {
        for (size_t j = 0; j < 8; j++) {
            if ((b->squares[i][j].piece_color == king_color) && (b->squares[i][j].piece_type == King)) {
                king_coord.x = i;
                king_coord.y = j;
                return king_coord;
            }
        }
    }
    assert(false && "no king found");
}

int orthogonal_checker(Board * b_to_check, int xo, int yo, Piece_type searched_piece, Piece_color searched_color,
                       Piece_color own_color) {
    Dir orthogonal[4] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
    for (size_t i = 0; i < 4; i++) {
        int dx = orthogonal[i].x, dy = orthogonal[i].y;
        for (size_t j = 1; j < 8; j++) {
            int xc = xo + j * dx, yc = yo + j * dy;
            if ((xc >= 0 && xc <= 7) && (yc >= 0 && yc <= 7)) {
                if (b_to_check->squares[xc][yc].piece_color == own_color) {
                    break;
                }
                if (b_to_check->squares[xc][yc].piece_type == searched_piece &&
                    b_to_check->squares[xc][yc].piece_color == searched_color) {
                    return 1;
                }
            }
        }
    }

    return 0;
}

int diagonal_checker(Board * b_to_check, int xo, int yo, Piece_type searched_piece, Piece_color searched_color,
                     Piece_color own_color) {

    Dir diagonal[4] = {{1, 1}, {-1, -1}, {1, -1}, {-1, 1}};
    for (size_t i = 0; i < 4; i++) {
        int dx = diagonal[i].x, dy = diagonal[i].y;
        for (size_t j = 1; j < 8; j++) {
            int xc = xo + j * dx, yc = yo + j * dy;
            if ((xc >= 0 && xc <= 7) && (yc >= 0 && yc <= 7)) {
                if (b_to_check->squares[xc][yc].piece_color == own_color) {
                    break;
                }
                if (b_to_check->squares[xc][yc].piece_type == searched_piece &&
                    b_to_check->squares[xc][yc].piece_color == searched_color) {
                    return 1;
                }
            }
        }
    }

    return 0;
}

int knight_checker(Board * b_to_check, int xo, int yo, Piece_type searched_piece, Piece_color searched_color) {
    Dir knight_magic[8] = {{2, 1}, {2, -1}, {-2, 1}, {-2, -1}, {1, 2}, {1, -2}, {-1, 2}, {-1, -2}};
    for (size_t i = 0; i < 8; i++) {
        int dx = knight_magic[i].x, dy = knight_magic[i].y;
        int xc = xo + dx, yc = yo + dy;
        if ((xc >= 0 && xc <= 7) && (yc >= 0 && yc <= 7)) {
            if (b_to_check->squares[xc][yc].piece_type == searched_piece &&
                b_to_check->squares[xc][yc].piece_color == searched_color) {
                return 1;
            }
        }
    }

    return 0;
}

int pawn_checker(Board * b_to_check, int xo, int yo, Piece_color opponent_color){
    Dir pawn_xdelta[2] = {{-1, 0}, {1, 0}};
    int orientation;
    if (opponent_color == 6){
        orientation = 1;
    }else {
        orientation = -1;
    }
    for (size_t i = 0; i<2; i++){
        int xc = xo + pawn_xdelta[i].x, yc = yo + orientation;
        if ((xc >= 0 && xc <= 7) && (yc >= 0 && yc <= 7)) {
            if (b_to_check->squares[xc][yc].piece_type == Pawn &&
                b_to_check->squares[xc][yc].piece_color == opponent_color){
                return 1;
            }
        }
    }
    return 0;

}

int is_king_safe(Board * b, Piece_color king_color, int xf, int yf, int xt, int yt) {
    Piece_color opponent_color = (king_color + 6)%6;
    Board * b_to_check = alloc_board();

    memcpy(b_to_check, b, sizeof(Board));

    b_to_check->squares[xt][yt].piece_type = b->squares[xf][yf].piece_type;
    b_to_check->squares[xt][yt].piece_color = b->squares[xf][yf].piece_color;

    b_to_check->squares[xf][yf].piece_type = Empty;
    b_to_check->squares[xf][yf].piece_color = Void;

    Location new_king_loc = king_finder(b_to_check, king_color);

    if (orthogonal_checker(b_to_check, new_king_loc.x, new_king_loc.y, Rook, opponent_color, king_color) == 1) {
        free_board(b_to_check);
        return 0;
    }
    if (orthogonal_checker(b_to_check, new_king_loc.x, new_king_loc.y, Queen, opponent_color, king_color) == 1) {
        free_board(b_to_check);
        return 0;
    }
    if (diagonal_checker(b_to_check, new_king_loc.x, new_king_loc.y, Bishop, opponent_color, king_color) == 1) {
        free_board(b_to_check);
        return 0;
    }
    if (diagonal_checker(b_to_check, new_king_loc.x, new_king_loc.y, Queen, opponent_color, king_color) == 1) {
        free_board(b_to_check);
        return 0;
    }
    if (knight_checker(b_to_check, new_king_loc.x, new_king_loc.y, Knight, opponent_color) == 1) {
        free_board(b_to_check);
        return 0;
    }
    if (pawn_checker(b_to_check, new_king_loc.x, new_king_loc.y, opponent_color) == 1) {
        free_board(b_to_check);
        return 0;
    }

    free_board(b_to_check);
    return 1;
}

int is_square_safe(Board * b, Piece_color own_color, int x, int y) {
    Piece_color opponent_color = (own_color + 6)%6;
    Board * b_to_check = alloc_board();

    memcpy(b_to_check, b, sizeof(Board));


    if (orthogonal_checker(b_to_check, x, y, Rook, opponent_color, own_color) == 1) {
        free_board(b_to_check);
        return 0;
    }
    if (orthogonal_checker(b_to_check, x, y, Queen, opponent_color, own_color) == 1) {
        free_board(b_to_check);
        return 0;
    }
    if (diagonal_checker(b_to_check, x, y, Bishop, opponent_color, own_color) == 1) {
        free_board(b_to_check);
        return 0;
    }
    if (diagonal_checker(b_to_check, x, y, Queen, opponent_color, own_color) == 1) {
        free_board(b_to_check);
        return 0;
    }
    if (knight_checker(b_to_check, x, y, Knight, opponent_color) == 1) {
        free_board(b_to_check);
        return 0;
    }
    if (pawn_checker(b_to_check, x, y, opponent_color) == 1) {
        free_board(b_to_check);
        return 0;
    }

    free_board(b_to_check);
    return 1;
}
