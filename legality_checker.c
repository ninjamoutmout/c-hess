#include "include/legality_checker.h"
#include "include/cell_lib.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

const Dir orthogonal[4] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
const Dir diagonal[4] = {{1, 1}, {-1, -1}, {1, -1}, {-1, 1}};
const Dir knight_magic[8] = {{2, 1}, {2, -1}, {-2, 1}, {-2, -1}, {1, 2}, {1, -2}, {-1, 2}, {-1, -2}};


void free_gen_returner(gen_returner *ret) {
    if (ret != NULL)
        free(ret);
}

static gen_returner *set_next_move(gen_returner *ret, int xt, int yt) {
    if (ret == NULL) {
        ret = malloc(sizeof(gen_returner));
        ret->count = 0;
    }
    ret = realloc(ret, sizeof(gen_returner) + (ret->count + 1) * sizeof(move_info));
    ret->data[ret->count].xt = xt;
    ret->data[ret->count].yt = yt;
    ret->count++;
    // Next.available = 0;
    return ret;
}
gen_returner *gen_piece_move(Board * b, int xf, int yf, Piece_color playing_color) {

    gen_returner *ret = NULL;

    int orientation;
    int starting_row;
    Piece_color opponent_color;

    if (playing_color == 0) {
        opponent_color = 6;
    } else {
        opponent_color = 0;
    }

    switch (b->squares[xf][yf].piece_type) {
    case Pawn: // Pawn (fucking anoying)
        if (b->squares[xf][yf].piece_color == 0) {
            orientation = 1;
            starting_row = 1;
        } else {
            orientation = -1;
            starting_row = 6;
        }
        int yt = yf + orientation;
        if ((yt <= 7) && (yt >= 0)) {

            if (b->squares[xf][yt].piece_color == Void) {
                ret = set_next_move(ret, xf, yt);
            }
            if (yf == starting_row && b->squares[xf][yt].piece_color == Void) {
                int ytt = yf + orientation * 2;
                if (b->squares[xf][ytt].piece_color == Void) {
                    ret = set_next_move(ret, xf, ytt);
                }
            }
        }


        break;
    case Knight: // Knight (also fucking annoying)
        for (size_t i = 0; i < 8; i++) {
            int dx = knight_magic[i].x, dy = knight_magic[i].y;
            int xt = xf + dx, yt = yf + dy;

            if ((xt <= 7) && (xt >= 0) && (yt <= 7) && (yt >= 0)) {
                if (b->squares[xt][yt].piece_color == playing_color) {
                    // we encountered nothing, we continue going and append this move
                } else if (b->squares[xt][yt].piece_color == Void) {
                    ret = set_next_move(ret, xt, yt);
                    // we encountered an opponent piece, we can go here but not futher, we append then we break
                } else {
                    ret = set_next_move(ret, xt, yt);
                }
            }
        }
        break;
    case Bishop: // Bishop (fine)
        for (size_t i = 0; i < 4; i++) {
            int dx = diagonal[i].x, dy = diagonal[i].y;
            for (int j = 1; j < 8; j++) {
                int xt = xf + j * dx, yt = yf + j * dy;
                if ((xt <= 7) && (xt >= 0) && (yt <= 7) && (yt >= 0)) {

                    // we encountered one of our own piece, cannot go futher : instant break
                    if (b->squares[xt][yt].piece_color == playing_color) {
                        // we encountered nothing, we continue going and append this move
                        break;
                    } else if (b->squares[xt][yt].piece_color == Void) {
                        // we encountered an opponent piece, we can go here but not futher, we append then we break
                        ret = set_next_move(ret, xt, yt);
                    } else {
                        ret = set_next_move(ret, xt, yt);
                        break;
                    }
                }
            }
        }
        break;
    case Rook: // Rook (fine)
        for (size_t i = 0; i < 4; i++) {
            int dx = orthogonal[i].x, dy = orthogonal[i].y;
            for (int j = 1; j < 8; j++) {
                int xt = xf + j * dx, yt = yf + j * dy;
                if ((xt <= 7) && (xt >= 0) && (yt <= 7) && (yt >= 0)) {
                    // we encountered one of our own piece, cannot go futher : instant break
                    if (b->squares[xt][yt].piece_color == playing_color) {
                        break;

                    // we encountered nothing, we continue going and append this move
                    } else if (b->squares[xt][yt].piece_color == Void) {
                        ret = set_next_move(ret, xt, yt);

                    // we encountered an opponent piece, we can go here but not futher, we append then we break
                    } else {
                        ret = set_next_move(ret, xt, yt);
                        break;
                    }
                    
                }
            }
        }
        break;
    case Queen: // Quenn (a tad annoying)
        for (size_t i = 0; i < 4; i++) {
            int dx = diagonal[i].x, dy = diagonal[i].y;
            for (int j = 1; j < 8; j++) {
                int xt = xf + j * dx, yt = yf + j * dy;
                if ((xt <= 7) && (xt >= 0) && (yt <= 7) && (yt >= 0)) {

                    // we encountered one of our own piece, cannot go futher : instant break
                    if (b->squares[xt][yt].piece_color == playing_color) {
                        break;

                        // we encountered nothing, we continue going and append this move
                    } else if (b->squares[xt][yt].piece_color == Void) {
                        ret = set_next_move(ret, xt, yt);
                    } else {
                        ret = set_next_move(ret, xt, yt);
                        break;
                    }
                }
            }
        }

        for (size_t i = 0; i < 4; i++) {
            int dx = orthogonal[i].x, dy = orthogonal[i].y;
            for (int j = 1; j < 8; j++) {
                int xt = xf + j * dx, yt = yf + j * dy;
                if ((xt <= 7) && (xt >= 0) && (yt <= 7) && (yt >= 0)) {

                    // we encountered one of our own piece, cannot go futher : instant break
                    if (b->squares[xt][yt].piece_color == playing_color) {
                        break;

                        // we encountered nothing, we continue going and append this move
                    } else if (b->squares[xt][yt].piece_color == Void) {
                        ret = set_next_move(ret, xt, yt);

                        // we encountered an opponent piece, we can go here but not futher, we append then we break
                    } else {
                        ret = set_next_move(ret, xt, yt);
                        break;
                    }
                }
            }
        }
        break;
    case King: // King (:dead:)
        for (size_t i = 0; i < 4; i++) {
            int dx = diagonal[i].x, dy = diagonal[i].y;
            int xt = xf + dx, yt = yf + dy;
            if ((xt <= 7) && (xt >= 0) && (yt <= 7) && (yt >= 0)) {

                // we encountered one of our own piece, cannot go futher : instant break
                if (b->squares[xt][yt].piece_color == playing_color) {
                    // we encountered nothing, we continue going and append this move
                } else if (b->squares[xt][yt].piece_color == Void) {
                    ret = set_next_move(ret, xt, yt);

                    // we encountered an opponent piece, we can go here but not futher, we append then we break
                } else {
                    ret = set_next_move(ret, xt, yt);
                }
            }
        }
        for (size_t i = 0; i < 4; i++) {
            int dx = orthogonal[i].x, dy = orthogonal[i].y;
            // for (int j = 1; j < 8; j++) {
            int xt = xf + dx, yt = yf + dy;
            if ((xt <= 7) && (xt >= 0) && (yt <= 7) && (yt >= 0)) {

                // we encountered one of our own piece, cannot go futher : instant break
                if (b->squares[xt][yt].piece_color == playing_color) {
                    printf("ORTHOGONAL : encountered own piece at x:%d y:%d \n", xt, yt);

                    // we encountered nothing, we continue going and append this move
                } else if (b->squares[xt][yt].piece_color == Void) {
                    // printf("ORTHOGONAL : encountered nothing at x:%d y:%d \n", xt, yt);

                    ret = set_next_move(ret, xt, yt);

                    // we encountered an opponent piece, we can go here but not futher, we append then we break
                } else {
                    // printf("ORTHOGONAL : encountered enemy piece at x:%d y:%d \n", xt, yt);
                    ret = set_next_move(ret, xt, yt);
                }
            }
            // }
        }
        break;
    case Empty:
        assert(false && "something went horibly wrong (you cannot move a not existing piece)");
        break;
    };

    return ret;
}
